import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        Port port = new Port(15, 2);
        Semaphore semForNumShips = new Semaphore(port.getNumJetty());
        Semaphore semForNumContainersInPort = new Semaphore(port.getCapacity());
        Thread firstThead = new Thread(new Ship(port, 6, 5, semForNumShips, semForNumContainersInPort));
        Thread secondThead = new Thread(new Ship(port, 6, 5, semForNumShips, semForNumContainersInPort));
        Thread thirdThead = new Thread(new Ship(port, 6, 5, semForNumShips, semForNumContainersInPort));
        firstThead.setName("Первое судно");
        firstThead.start();
        secondThead.setName("Второе судно");
        secondThead.start();
        thirdThead.setName("Третье судно");
        thirdThead.start();
    }
}
