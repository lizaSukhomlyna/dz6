import java.util.concurrent.Semaphore;

public class Ship implements Runnable {
    private Port portDestination;
    private int capacity;
    private int containers;
    private Semaphore semForNumContainersOnShip;
    private Semaphore semForNumShips;
    private Semaphore semForNumContainers;

    public Ship(Port portDestination, int capacity, int containers, Semaphore semForNumShips, Semaphore semForNumContainers) {
        this.portDestination = portDestination;
        this.capacity = capacity;
        this.containers = containers;
        this.semForNumShips = semForNumShips;
        this.semForNumContainers = semForNumContainers;
        this.semForNumContainersOnShip = new Semaphore(capacity);
    }

    @Override
    public void run() {
        try {
            this.semForNumShips.acquire();
            this.portDestination.numShipsIn++;
            System.out.printf("\n%s %s %d", Thread.currentThread().getName(), "зашло в порт.\nОбщее количество суден в порту:", portDestination.numShipsIn);
            int numContainersInPort = this.containers;
            for (int i = 0; i < numContainersInPort; i++) {
                this.semForNumContainers.acquire();
                this.portDestination.containers++;
                this.semForNumContainersOnShip.acquire();
                this.containers--;
                System.out.printf("\n%s %s %d", Thread.currentThread().getName(), "отгрузило контейнер.\nОбщее количество контейнеров в порту:", portDestination.containers);
                System.out.printf("%s %s: %d", "\nОбщее количество контейнеров в", Thread.currentThread().getName(), this.containers);
            }
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        for (int i = 0; i < this.capacity; i++) {
            if (this.portDestination.containers != 0) {
                this.portDestination.containers--;
                this.containers++;
                System.out.printf("\n%s %s %d", Thread.currentThread().getName(), "загрузило контейнер.\nОбщее количество контейнеров в порту:", portDestination.containers);
                System.out.printf("%s %s %d", "\nОбщее количество контейнеров в", Thread.currentThread().getName(), this.containers);
            }
        }
        portDestination.numShipsIn--;
        System.out.printf("\n%s %s", Thread.currentThread().getName(), "вышло из порта");
        semForNumShips.release();
        semForNumContainers.release();
        semForNumContainersOnShip.release();
    }
}