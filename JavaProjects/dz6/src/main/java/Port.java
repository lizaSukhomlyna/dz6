import lombok.Data;

@Data
public class Port {
    private int capacity;
    private int numJetty;
    int containers;
    int numShipsIn;

    public Port(int capacity, int numJetty) {
        this.capacity = capacity;
        this.numJetty = numJetty;
        this.containers = 0;
        this.numShipsIn = 0;
    }
}
